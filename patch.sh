echo "Patching frameworks/base"
cd frameworks/base
git remote add patch https://github.com/nostumato/android_frameworks_base
git fetch patch
git cherry-pick 3371226392204aea144ebaf7953dc2631e95e1da ed3748ba4fe3a32e7f2dbf791a292274e45f5eea e9c5b2ef86b4d92c463fc676960da57b0c3f7531 1f9b02de6151a9e42ccae864250fcde99efd04ca 528d377313db3a533e4e33fd40220875ae0acdd1 dd3920b7326a138cb2b5215ff5eb9f2bea626596
git reset nos/o2
cd ../../
echo "Patching system/core"
cd system/core
git remote add patch https://github.com/nostumato/android_system_core
git fetch patch
git cherry-pick 0abfbc7d1f5b41d356fab525df5b14b95a8c894b ee77b7837d4555cdad0a633cdd1e9163444dfcda 40b36f2f3c96f81ec9074016e6eadf9c2e7ba9c2 c0c3cf270b663d6b9a6180f736d79c4c7d190e17 cd0fb104e7d82fccee7ffd47cca6092d9d7b9fc2 6c998d335f51fabe201e38978395d6525f8a2e12
git fetch https://github.com/AospExtended/platform_system_core/ && git cherry-pick 7818d7eb56289ff2a7a63163f6e2b8aac5397efe
git reset nos/o2
cd ../../
echo "Patching system/media"
cd system/media
git remote add patch https://github.com/nostumato/android_system_media
git fetch patch
git cherry-pick 80d3d53addcfaaa6cb805de349741dd033d87b0d c8ec46eac8de3f583d98d3d8d871db959e5d7b0c
git reset nos/o2
cd ../../
echo "Patching system/extras"
cd system/extras
git fetch https://github.com/AospExtended/platform_system_extras/ && git cherry-pick bfe60794b38a772fc6922308293711bd1457ef76
git reset nos/o2
cd ../../
echo "Patching vendor/nitrogen"
cd vendor/nitrogen
git remote add patch https://github.com/nostumato/android_vendor_nitrogen/
git fetch patch
git cherry-pick 90179455fbfec0804ad4c3b7cd71d8a7f4781ecd 3bdfb1aee3279eae28a61de004b015a270775e40
git reset nos/o2
cd ../../
echo "Patching build/make"
cd build/make
git remote add patch https://github.com/nitro-athene/android_build_make/
git fetch patch
git cherry-pick 4b491f9947b7ce4b27824d2377aa5f4128188c34 deeaa4a8ac4e2a10143491a0c48526bf0afbb9b0 703c101f9f14f02fa149d08ab7e0e2fec8fff876 9ba159c557d74ece166ffc5a3a2e0b1d594fefa1 713fe21c42a4e0698b0a75155ccba1178175588e 69a85d64897577af6ccf0f1eb61df3a16eb698e3 8ff3ab5bc61537aebf9572b0bd58860093d81eb0 e52ca6f7847f1f4918b669bca1b598b5a0f572ac 3f457e933c89faad31b9dd13367466f0bfc7b4e8 3910f000c2a707ce04f89f7f9221d46b766b882e 855412eb05ef4de592560d0cb8e44f8ed123ac36 da01e391179b1e537d37c31f3f66386ff3e8fec0 9d3e98135531d8d6d7ffba05ded7e0a8beeaa6b5 8abd4f82794099674850c430a6ca236a08d90ebb 9ef05934ad4941551edd8e17546c1a5f4970eeff 5f03ee6d99c9f17dd998cec345d51aa892145f98 ba9a7a3b2c3e2fdc5e0125e509f44f2d30c88fa5
git fetch https://github.com/nostumato/android_build_make/ && git cherry-pick 388953db33ed23570130b72ccec423a2adcf4c23 12da319b4471e914f352bdea338c1b95b6ebab3e 9ad93cdba02c3f80ba49b0e4be0428d93f482035
git reset nos/o2
cd ../
echo "Patching build/soong"
cd soong
git remote add patch https://github.com/nitro-athene/android_build_soong
git fetch patch
git cherry-pick 1213ed6ae525d3bb4ccd3260f1b15edaa09984cc 4323d1bfc8cdb4a884ab01b2016ad3b5b69b84c4 f5fdc40d78dab6df3f4c36d5875701f30f20890e
git reset nos/o2
cd ../../

rm -rf hardware/libhardware_legacy
git clone https://github.com/nostumato/android_hardware_libhardware_legacy.git -b o2 hardware/libhardware_legacy
echo "Patch Completed"
