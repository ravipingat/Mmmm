echo "Cloning Device Tree"
git clone https://gitlab.com/ravipingat/Mido3.git -b o3 device/xiaomi/mido

echo "Cloning Kernel Tree"
git clone https://github.com/Adesh15/android_kernel_xiaomi_msm8953.git -b oreo kernel/xiaomi/msm8953

rm -rf vendor/xiaomi

echo "Cloning Vendor Tree"
git clone https://github.com/adesh15/proprietary_vendor_xiaomi.git -b lineage-15.1 vendor/xiaomi

rm -rf hardware/qcom/bt
rm -rf hardware/qcom/wlan
rm -rf hardware/qcom/power
rm -rf hardware/qcom/keymaster
rm -rf hardware/qcom/audio*
rm -rf hardware/qcom/media*
rm -rf hardware/qcom/display*
rm -rf hardware/ril
rm -rf system/bt

echo "Cloning Some Caf repos"
git clone https://github.com/GZOSP/hardware_qcom_audio.git -b 8.1 hardware/qcom/audio
git clone https://github.com/GZOSP/hardware_qcom_media.git -b 8.1 hardware/qcom/media
git clone https://github.com/GZOSP/hardware_qcom_display.git -b 8.1 hardware/qcom/display
git clone https://github.com/GZOSP/hardware_qcom_bt.git -b 8.1-caf hardware/qcom/bt-caf
git clone https://github.com/GZOSP/hardware_qcom_bt.git -b 8.1 hardware/qcom/bt
git clone https://github.com/GZOSP/hardware_qcom_wlan.git -b 8.1-caf hardware/qcom/wlan-caf
git clone https://github.com/GZOSP/hardware_qcom_wlan.git -b 8.1 hardware/qcom/wlan
git clone https://github.com/GZOSP/hardware_qcom_power.git -b 8.1 hardware/qcom/power
git clone https://github.com/GZOSP/hardware_qcom_keymaster.git -b 8.1 hardware/qcom/keymaster
git clone https://github.com/KudProject/hardware_qcom_audio.git -b oreo-mr1-caf-8996 hardware/qcom/audio-caf/msm8937
git clone https://github.com/KudProject/hardware_qcom_media.git -b oreo-mr1-caf-8996 hardware/qcom/media-caf/msm8937
git clone https://github.com/KudProject/hardware_qcom_display.git -b oreo-mr1-caf-8996 hardware/qcom/display-caf/msm8937
git clone https://github.com/PixelExperience/system_bt.git -b oreo-mr1 system/bt
git clone https://github.com/PixelExperience/hardware_ril.git -b oreo-mr1 hardware/ril
git clone https://github.com/PixelExperience/hardware_ril-caf.git -b oreo-mr1 hardware/ril-caf
git clone https://github.com/Rpingat/vendor_extras.git -b o8x vendor/extras
rm -rf vendor/extras/config*
rm -rf vendor/extras/tools
git clone https://github.com/LineageOS/android_device_qcom_common.git -b lineage-15.1 device/qcom/common
rm -rf system/vold
git clone https://github.com/AospExtended/platform_system_vold.git -b 8.1.x system/vold
git clone https://github.com/AospExtended/platform_vendor_qcom_opensource_cryptfs_hw.git -b 8.1.x vendor/qcom/opensource/cryptfs_hw
git clone https://github.com/AospExtended/platform_system_qcom.git -b 8.1.x system/qcom
git clone https://github.com/AospExtended/platform_external_libncurses.git -b 8.1.x external/libncurses
git clone https://github.com/AospExtended/platform_external_powertop.git -b 8.1.x external/powertop
git clone https://github.com/AospExtended/platform_external_brctl.git -b 8.1.x external/brctl
rm -rf external/libnetfilter_conntr*
git clone https://github.com/AospExtended/platform_external_libnetfilter_conntrack.git -b 8.1.x external/libnetfilter_conntrack
rm -rf vendor/qcom/openso*/fm
git clone https://github.com/gzosp/packages_apps_FMRadio -b 8.1 packages/apps/FMRadio
git clone https://github.com/gzosp/hardware_qcom_fm -b 8.1 hardware/qcom/fm
rm -rf hardware/libhar*legacy
git clone https://github.com/nostumato/android_hardware_libhardware_legacy.git -b o2 hardware/libhardware_legacy
rm -rf system/core
rm -rf system/media
rm -rf system/extras
rm -rf system/libhidl
git clone https://github.com/nitro-athene/android_system_core.git -b 8.1 system/core
git clone https://github.com/nitro-athene/android_system_extras.git -b o2 system/extras
git clone https://github.com/nitro-athene/android_system_media.git -b o2 system/media
git clone https://github.com/Team-Xtreme/system_libhidl.git -b o8.1 system/libhidl
git clone https://github.com/AospExtended/platform_packages_apps_Snap.git -b 8.1.x packages/apps/Snap
git clone git://github.com/krasCGQ/aarch64-linux-android -b opt-gnu-7.x prebuilts/gcc/linux-x86/aarch64/aarch64-linux-android-7.x
echo "Completed"
